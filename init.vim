call plug#begin("~/.vim/plugged")
	Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	Plug 'vim-airline/vim-airline-themes'
	Plug 'peitalin/vim-jsx-typescript'
	Plug 'leafgarland/typescript-vim'
	Plug 'maxmellon/vim-jsx-pretty'
	Plug 'preservim/nerdcommenter'
	Plug 'vim-airline/vim-airline'
	Plug 'ryanoasis/vim-devicons'
	Plug 'jiangmiao/auto-pairs'
	Plug 'frazrepo/vim-rainbow'
	Plug 'scrooloose/nerdtree'
	Plug 'junegunn/fzf.vim'
	Plug 'morhetz/gruvbox'
	Plug 'ayu-theme/ayu-vim'
	Plug 'aditya-azad/candle-grey'
	"Plug 'dylanaraps/wal.vim'
	"Plug 'dracula/vim'
	Plug 'tpope/vim-surround'
	Plug 'Jorengarenar/vim-MvVis'

call plug#end()

"#######################################################
"Coc
	let g:coc_global_extensions = ['coc-emmet','coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver','coc-sh','coc-clangd', 'coc-pyright']

	set mouse=a
	
	vmap <C-c> "+y

	"au BufWinLeave *.js mkview
	"au BufWinEnter *.js loadview 
	au BufWinEnter *.js set foldmethod=manual


"MvVis
"
	let g:MvVis_mappings=0
	vmap <C-left> <Plug>(MvVisLeft)
	vmap <C-down> <Plug>(MvVisDown)
	vmap <C-up> <Plug>(MvVisUp)
	vmap <C-right> <Plug>(MvVisRight)
"Gruv

	let g:gruvbox_italic=1
	let g:gruvbox_italicize_strings=1
	let g:gruvbox_invert_selection=0
	let g:gruvbox_contrast_dark="soft"
	let g:gruvbox_sign_column='bg0'
	let g:gruvbox_transparent_bg='0'
	colorscheme gruvbox
	
	"Ayu Color Scheme
	"set termguicolors 
	"colorscheme ayu 
	"let ayucolor="dark"
	
	"Dracula Color Scheme
	"colorscheme dracu
	
	"Wal Color Scheme
	"colorscheme wal 
	
	" Candle Color Scheme
	"set t_Co=256 " enable colors in terminal
	"colorscheme candle-grey
	"Or
	"colorscheme candle-grey

	 au BufWinEnter * syntax on
	 au BufWinEnter * highlight Normal ctermbg=0

	let g:rainbow_active = 1

"unicode symbols
	let g:airline_powerline_fonts = 1

	if !exists('g:airline_symbols')
	    let g:airline_symbols = {}
	endif

	let g:airline_left_sep = '»'
	let g:airline_left_sep = '▶'
	let g:airline_right_sep = '«'
	let g:airline_right_sep = '◀'
	let g:airline_symbols.linenr = '␊'
	let g:airline_symbols.linenr = '␤'
	let g:airline_symbols.linenr = '¶'
	let g:airline_symbols.branch = '⎇'
	let g:airline_symbols.paste = 'ρ'
	let g:airline_symbols.paste = 'Þ'
	let g:airline_symbols.paste = '∥'
	let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
	let g:airline_left_sep = ''
	let g:airline_left_alt_sep = ''
	let g:airline_right_sep = ''
	let g:airline_right_alt_sep = ''
	let g:airline_symbols.branch = ''
	let g:airline_symbols.readonly = ''
	let g:airline_symbols.linenr = ''

"Airline
	let g:airline_theme='gruvbox'

	"let g:airline_theme='dracula'


"Nerdtree	
	let g:NERDTreeShowHidden = 1
	let g:NERDTreeMinimalUI = 0
	let g:NERDTreeIgnore = []
	let g:NERDTreeStatusline = ''
	
"Toggle
	nnoremap <silent> <C-b> :NERDTreeToggle<CR>

"NumberLine
	set nu
	set relativenumber

"open terminal on ctrl+n
	function! OpenTerminal()
	  set splitbelow
	  split term://bash
	  resize 8
	endfunction
	nnoremap <c-n> :call OpenTerminal()<CR>


"Functions 

	function! QuickSave()
		:w!
		":CocCommand prettier.formatFile
	endfunction
	nnoremap <c-c> :call QuickSave()<CR>

	function! CleanExit()
		:q!
	endfunction
	nnoremap <c-z> :call CleanExit()<CR>

	"command! -nargs=0 Prettier 
	

" use alt+hjkl to move between split/vsplit panels
	tnoremap <A-h> <C-\><C-n><C-w>h
	tnoremap <A-j> <C-\><C-n><C-w>j
	tnoremap <A-k> <C-\><C-n><C-w>k
	tnoremap <A-l> <C-\><C-n><C-w>l
	nnoremap <A-left> <C-w>h
	nnoremap <A-down> <C-w>j
	nnoremap <A-up> <C-w>k
	nnoremap <A-right> <C-w>l
	nnoremap <C-Left> :tabprevious<CR>                                                                            
	nnoremap <C-Right> :tabnext<CR>


	nnoremap <C-p> :FZF<CR>
	let g:fzf_action = { 'ctrl-t': 'tab split', 'ctrl-s': 'split', 'ctrl-v': 'vsplit'}

	  let g:fzf_preview_window = ['up:40%:hidden', 'ctrl-/']


	function! s:check_back_space() abort
	  let col = col('.') - 1
	  return !col || getline('.')[col - 1]  =~ '\s'
	endfunction


"Coc

	inoremap <silent><expr> <c-space> coc#refresh()

	inoremap <silent><expr> <Tab>
      	\ pumvisible() ? "\<C-n>" :
      	\ <SID>check_back_space() ? "\<Tab>" :
      	\ coc#refresh()



	inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
	inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
